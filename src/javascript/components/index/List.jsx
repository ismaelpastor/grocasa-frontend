import React from "react";
import { Grid, Container } from "@material-ui/core";
import Item from "./Item";
import images from "../../../images/Rectangle -1@2x.png";
import logo from "../../../images/Grocasa.png";

const listItems = [
    {
        id: 1,
        name: "Ático Dúplex de Obra Nueva",
        address: "Marianao, Sant Boi de Llobregat",
        constructedMeters: 102,
        usefulMeters: 100,
        bedrooms: 3,
        bathrooms: 2,
        price: 330000,
        type: "ático",
        category: "segunda mano",
        state: "reformado",
        description:
            "Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas.",
        extras: [
            "terraza",
            "balcón",
            "ascensor",
            "calefacción",
            "aire acondicionado",
        ],
        images,
    },
    {
        id: 2,
        name: "Ático Dúplex de Obra Nueva",
        address: "Marianao, Sant Boi de Llobregat",
        constructedMeters: 102,
        usefulMeters: 100,
        bedrooms: 3,
        bathrooms: 2,
        price: 330000,
        type: "ático",
        category: "segunda mano",
        state: "reformado",
        description:
            "Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas.",
        extras: [
            "terraza",
            "balcón",
            "ascensor",
            "calefacción",
            "aire acondicionado",
        ],
        images,
    },
    {
        id: 3,
        name: "Ático Dúplex de Obra Nueva",
        address: "Marianao, Sant Boi de Llobregat",
        constructedMeters: 102,
        usefulMeters: 100,
        bedrooms: 3,
        bathrooms: 2,
        price: 330000,
        type: "ático",
        category: "segunda mano",
        state: "reformado",
        description:
            "Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas.",
        extras: [
            "terraza",
            "balcón",
            "ascensor",
            "calefacción",
            "aire acondicionado",
        ],
        images,
    },
    {
        id: 4,
        name: "Ático Dúplex de Obra Nueva",
        address: "Marianao, Sant Boi de Llobregat",
        constructedMeters: 102,
        usefulMeters: 100,
        bedrooms: 3,
        bathrooms: 2,
        price: 330000,
        type: "ático",
        category: "segunda mano",
        state: "reformado",
        description:
            "Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas.",
        extras: [
            "terraza",
            "balcón",
            "ascensor",
            "calefacción",
            "aire acondicionado",
        ],
        images,
    },
];

const List = () => (
    <Container>
        <img
            src={logo}
            alt="Grocasa Logo"
            width="135"
            height="46"
        />
        <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={7}>
            {listItems.map((item) => (
                <Grid key={item.id} item sm={12} md={6} >
                    <Item item={item} />
                </Grid>
            ))}
        </Grid>
    </Container>
);

export default List;
