import React from "react";
import { Link } from 'react-router-dom';
import { Grid, Typography, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { ReactComponent as IconBathroom } from "../../../images/WEB_MATERIAL-09.svg"
import { ReactComponent as IconBedroom } from "../../../images/WEB_MATERIAL-10.svg"
import { ReactComponent as IconMeters } from "../../../images/WEB_MATERIAL-11.svg"

const OrangeButton = withStyles({
  root: {
    color: "#FE6D25",
    border: "2px solid #FE6D25",
    borderRadius: "20px",
    padding: "5px 30px",
    textTransform: "capitalize",
    textDecoration: "none"
  }
})(Button);

const Item = (props) => (


  <Grid container justify="space-evenly">
    <Grid item sm={12}>
      <img src={props.item.images} width="100%" alt="Item" />
      <Typography className="name">{props.item.name}</Typography>
      <Typography className="address">{props.item.address}</Typography>
    </Grid>
    <Grid container>
      <Grid item sm={2} align="center">
        <IconMeters width="40px" height="40px" /><Typography className="values">{props.item.constructedMeters} m<sup>2</sup></Typography>
      </Grid>
      <Grid item sm={2} align="center">
        <IconBedroom width="40px" height="40px" />
        <Typography className="values">{props.item.bedrooms}</Typography>
      </Grid>
      <Grid item sm={2} align="center">
        <IconBathroom width="40px" height="40px" />
        <Typography className="values">{props.item.bathrooms}</Typography>
      </Grid>
      <Grid item sm={3} align="center"><Typography className="price">{props.item.price}€</Typography></Grid>
      <Grid item sm={3}>
        <Link to={`detail/${props.item.id}`}>
          <OrangeButton>Más info</OrangeButton>
        </Link>
      </Grid>
    </Grid>
  </Grid>
);

export default Item;
