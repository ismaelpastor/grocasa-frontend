import React from "react";
import { Link } from 'react-router-dom';
import { Container } from "@material-ui/core";
import { Carousel } from "react-responsive-carousel";
import logo from "../../../images/Grocasa.png";
import imageMap from "../../../images/Screenshot 2020-07-06 at 11.53.49@2x.png";
import { ReactComponent as IconBack } from "../../../images/ICON_ARROW-05.svg"
import { Grid, Typography } from "@material-ui/core";
import { ReactComponent as IconBathroom } from "../../../images/WEB_MATERIAL-09.svg"
import { ReactComponent as IconBedroom } from "../../../images/WEB_MATERIAL-10.svg"
import { ReactComponent as IconMeters } from "../../../images/WEB_MATERIAL-11.svg"


const item =
{
    id: 1,
    name: "Ático Dúplex de Obra Nueva",
    address: "Marianao, Sant Boi de Llobregat",
    constructedMeters: 102,
    usefulMeters: 100,
    bedrooms: 3,
    bathrooms: 2,
    price: 330000,
    type: "ático",
    category: "segunda mano",
    state: "reformado",
    description:
        "Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas. Dúplex de obra seminueva en Marianao. Finca del 2019. El piso consta de una planta con entrada al salón y final en terraza. El salón da entrada a la cocina y al pasillo que distribuye una de las habitaciones dobles y uno de los cuartos. de baño de tres piezas.",
    extras: [
        "terraza",
        "balcón",
        "ascensor",
        "calefacción",
        "aire acondicionado",
    ]
};


const Detail = () => (
    <Container>
        <Link to={`/`}>
            <IconBack width="22px" height="22px" />
        </Link>
        <img
            src={logo}
            alt="Grocasa Logo"
            width="135"
            height="46"
        />

        <Carousel showThumbs={false} showStatus={false}>
            <div>
                <img src="../Rectangle-3@2x.png" alt="Foto 1" />
                <p className="legend">Foto 1</p>
            </div>
            <div>
                <img src="../Rectangle-3@2x.png" alt="Foto 2" />
                <p className="legend">Foto 2</p>
            </div>
            <div>
                <img src="../Rectangle-3@2x.png" alt="Foto 3" />
                <p className="legend">Foto 3</p>
            </div>
            <div>
                <img src="../Rectangle-3@2x.png" alt="Foto 4" />
                <p className="legend">Foto 4</p>
            </div>
            <div>
                <img src="../Rectangle-3@2x.png" alt="Foto 5" />
                <p className="legend">Foto 5</p>
            </div>

        </Carousel>

        <Grid container>
            <Grid item sm={12}>
                <Typography className="name">{item.name}</Typography>
                <Typography className="address">{item.address}</Typography>
                <Typography className="address">{item.description}</Typography>
            </Grid>
            <Grid container>
                <Grid item sm={2} align="center">
                    <IconMeters width="40px" height="40px" /><Typography className="values">{item.constructedMeters} m<sup>2</sup></Typography>
                </Grid>
                <Grid item sm={2} align="center">
                    <IconBedroom width="40px" height="40px" />
                    <Typography className="values">{item.bedrooms}</Typography>
                </Grid>
                <Grid item sm={2} align="center">
                    <IconBathroom width="40px" height="40px" />
                    <Typography className="values">{item.bathrooms}</Typography>
                </Grid>
                <Grid item sm={3} align="center"><Typography className="price">{item.price}€</Typography></Grid>
            </Grid>
            <Grid item sm={12} md={6} align="left">
                <img src={imageMap} width="100%" alt="Map"/>
            </Grid>
        </Grid>

    </Container>
);

export default Detail;



