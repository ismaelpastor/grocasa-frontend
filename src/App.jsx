import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import List from './javascript/components/index/List'
import Detail from './javascript/components/detail/Detail'
import './App.scss';

const App = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={List} />
            <Route path="/detail/:id" component={Detail} />
        </Switch>
    </Router>

);

export default App;
